# Copyright 2011-2013, The Trustees of Indiana University and Northwestern
#   University.  Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed
#   under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
#   CONDITIONS OF ANY KIND, either express or implied. See the License for the
#   specific language governing permissions and limitations under the License.
# ---  END LICENSE_HEADER BLOCK  ---

class solr::config inherits solr {

  File {
    selinux_ignore_defaults => true
  }

  file { 'solr.xml':
    ensure  => file,
    owner   => $user,
    group   => $group,
    content => template('solr/solr.xml.erb'),
    path    => "$tomcat_webapps_conf/solr.xml",
    require => Class['solr::install'],
  }

  file { "$solr_home":
    ensure  => directory,
    owner   => $user,
    group   => $group,
  }

  file {"$solr_home/solr.xml":
    ensure  => present,
    owner   => $user,
    group   => $group,
    content => template('solr/solr-cores.xml.erb'),
    require => File["$solr_home"],
  }
}
