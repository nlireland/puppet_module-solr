# Copyright 2011-2013, The Trustees of Indiana University and Northwestern
#   University.  Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed
#   under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
#   CONDITIONS OF ANY KIND, either express or implied. See the License for the
#   specific language governing permissions and limitations under the License.
# ---  END LICENSE_HEADER BLOCK  ---

class solr::install inherits solr {
  include solr::config
  include staging

  staging::file { "solr-$solr_version.tgz":
    source  => $solr_download_url,
    timeout => 1200,
    subdir  => solr,
    ##TODO Should probably go into a tomcat module specific to our tomcat package
    require => Class['tomcat'],
    notify  => Service['tomcat'],
  }

  staging::extract { "solr-$solr_version.tgz":
    target  => "${staging::path}/solr",
    creates => "${staging::path}/solr/solr-$solr_version",
    require => Staging::File["solr-$solr_version.tgz"],
  }

  file { "$tomcat_webapps/solr.war":
    source  => "${staging::path}/solr/solr-$solr_version/dist/solr-$solr_version.war",
    owner   => $user,
    group   => $group,
    require => [Staging::Extract["solr-$solr_version.tgz"],File[$solr_home]],
  }

  file { ["$solr_home/lib"]:
    ensure  => directory,
    owner   => $user,
    group   => $group,
    recurse => true,
    purge   => false,
    sourceselect => all,
    source  => ["${staging::path}/solr/solr-$solr_version/example/lib/ext/", "${staging::path}/solr/solr-$solr_version/dist/"],
    require => [Staging::Extract["solr-$solr_version.tgz"],File[$solr_home]],
  }

  file { ["$solr_home/lib/contrib"]:
    ensure  => directory,
    owner   => $user,
    group   => $group,
    require => [File["$solr_home/lib"]],
  }

  class { 'solr::contrib':
    solr_version    => $solr_version,
    require => [Staging::Extract["solr-$solr_version.tgz"],File[$solr_home],File["$solr_home/lib/contrib"]],
  }
}
