define solr::core ($solr_core = $title) {
  include solr
  include solr::params

  file { "$solr_core":
    ensure  => directory,
    owner   => $solr::group,
    group   => $solr::group,
    path    => "$solr::solr_home/$solr_core",
    require => File["$solr::solr_home"],
  }

  file { "$solr::solr_home/$solr_core/core.properties":
    ensure  => present,
    content  => "name=$solr_core",
    owner   => $solr::group,
    group   => $solr::group,
    require => File["$solr_core", "conf_$solr_core"],
    notify  => Service["tomcat"],
  }
  file { "conf_$solr_core":
    ensure  => directory,
    path    => "$solr::solr_home/$solr_core/conf",
    source  => "puppet:///modules/solr/$solr_core/conf",
    owner   => $solr::params::user,
    group   => $solr::params::group,
    recurse => true,
    require => [File["$solr_core"],File["$solr::tomcat_webapps/solr.war"],Class['tomcat']],
  }

  exec { "reload-$solr_core":
    command => "/usr/bin/curl 'http://localhost:$solr::tomcat_http_port/solr/admin/cores?action=RELOAD&core=$solr_core'",
    subscribe => File["conf_$solr_core"],
    refreshonly => true,
  }
}
