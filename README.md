# README #

This is a Solr repositry which is a highly reliable, scalable and fault tolerant, providing distributed indexing, replication and load-balanced querying, automated failover and recovery, centralized configuration.

*Note: This repository was originally forked from:* https://github.com/avalonmediasystem/puppet-solr


### Set up ###

Add the module reference to the Puppetfile of Puppet-Master in order to use this module 
```
mod 'nli-solr',
  :git => 'https://bitbucket.org/nlireland/puppet_module-solr'
```

### If Adding Additional Cores ###
* templates/solr-cores.xml.erb - Add additional core specific details to the <cores> tag and link to the params.pp values
* manifests/config.pp - Add additional files for new core
* files/MODULE/conf - Create new MOUDLE folder for each core containing a conf directory with a schema.xml and solrconfig.xml file