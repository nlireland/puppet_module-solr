<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:cpr="http://www.nli.ie/cpr/" >
  <xsl:output method="xml" indent="yes"/>
  <xsl:template match="/">
    <add>
      <xsl:for-each select="cpr:register/cpr:page">
        <doc>
          <field name="id"><xsl:value-of select="cpr:register_id" /><xsl:text>_</xsl:text><xsl:value-of select="format-number(cpr:register_sequence_number, '000')" /></field>
          <field name="doc_type"><xsl:text>page</xsl:text></field>
          <field name="reel_id_ssi"><xsl:value-of select="cpr:reel_id" /></field>
          <field name="reel_frame_number_isi"><xsl:value-of select="cpr:reel_frame_number" /></field>
          <field name="reel_register_position_isi"><xsl:value-of select="cpr:reel_register_position" /></field>
          <field name="previous_image_id_ssi"><xsl:value-of select="cpr:previous_image_id" /></field>
          <field name="register_id_ssi"><xsl:value-of select="cpr:register_id" /></field>
          <field name="register_sequence_number_isi"><xsl:value-of select="cpr:register_sequence_number" /></field>
          <xsl:if test="cpr:events">
            <xsl:for-each select="cpr:events/cpr:event">
              <field name="event_tesim"><xsl:value-of select="." /></field>
              <field name="event_ssim"><xsl:value-of select="." /></field>
            </xsl:for-each>
          </xsl:if>
          <xsl:if test="cpr:dates">
            <xsl:for-each select="cpr:dates/cpr:date">
              <field name="date_ssim"><xsl:value-of select="." /></field>
              <field name="year_isim"><xsl:value-of select="substring-before(.,'-')" /></field>
              <field name="month_isim"><xsl:value-of select="number(substring-after(.,'-'))" /></field>
            </xsl:for-each>
          </xsl:if>
          <field name="is_index_bsi"><xsl:value-of select="cpr:is_index" /></field>
          <xsl:if test="cpr:content_note">
            <xsl:for-each select="cpr:content_note">
              <field name="content_note_ssim"><xsl:value-of select="." /></field>
            </xsl:for-each>
          </xsl:if>
          <xsl:if test="cpr:is_duplicate_of">
            <xsl:for-each select="cpr:is_duplicate_of">
              <field name="is_duplicate_of_ssim"><xsl:value-of select="." /></field>
            </xsl:for-each>
          </xsl:if>
          <field name="is_lead_sheet_bsi"><xsl:value-of select="cpr:is_lead_sheet" /></field>
          <!-- unmapped fields
          <xsl:if test="pr:leadSheetContents">
            <lead_sheet_contents>
              <xsl:for-each select="pr:leadSheetContents/pr:daterange">
                <daterange><xsl:value-of select="." /></daterange>
              </xsl:for-each>
              <lead_sheet_transcription><xsl:value-of select="pr:leadSheetContents/pr:leadSheetTranscription" /></lead_sheet_transcription>
            </lead_sheet_contents>
           </xsl:if>
          -->
        </doc>
      </xsl:for-each>
    </add>
  </xsl:template>
</xsl:stylesheet>
